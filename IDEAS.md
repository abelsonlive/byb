
- Facebook Privacy Violation Article Generator:
  * Two Neural Nets, One trained on articles about facebook privacy violations, one trained on Mark Zuckerburg's Face.
- Surveillance bot
  * [Earthcam](https://www.earthcam.com/) + [YOLO](https://pjreddie.com/darknet/yolo/)
- Oulipo Bots
  * http://www.spoonbill.org/n+7/
  * https://en.wikipedia.org/wiki/Constrained_writing

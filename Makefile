
install: ## install the library for local development
	pip install -r requirements-dev.txt -r requirements.txt
	pip install -e .

test: ## run tests via pytest
	pytest -vv

docs-html: ## build documentation with pdoc
	PDOC_ALLOW_EXEC=1 pdoc --search -t docs/template -d google --logo /assets/spiral.png --logo-link / --favicon /assets/spiral.png  byb -o ./public
	cp -R assets/ public/assets

docs-view: ## view docs in a local server
	cd public && python -m http.server 3030

pypi:  ## push code to pypi
	rm -rf dist || true
	python setup.py sdist
	twine upload dist/*

classify_phonemes:  ## run the phoneme classification program
	./scripts/classify_phonemes.py